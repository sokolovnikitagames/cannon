using UnityEngine;
using UnityEngine.UIElements;
using Zenject;

public class CannonInstaller : MonoInstaller
{
    [SerializeField] private CannonBase _cannonPrefab;
    [SerializeField] private Transform _cannonSpawnPoint;

    private CannonBase _cannon;

    public override void InstallBindings()
    {
        SpawnCannon();
        BindCannonTransform();
        BindCannonBuff();
        BindCannonControllable();
        BindCannonReloadingEvent();
        BindCannonTransferable();
        BindCannonGameObject();
        BindCannonGetDamageable();
        BindCannonDiedEvent();
        BindCannonChangedHealthsEvent();
    }

    private void SpawnCannon()
    {
        _cannon = Container.InstantiatePrefabForComponent<CannonBase>
            (_cannonPrefab, _cannonSpawnPoint.position, Quaternion.identity, null);
    }

    private void BindCannonTransform()
    {       
        Container
            .Bind<ICannonTransform>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonBuff()
    {
        Container
            .Bind<ICannonBuffable>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonControllable()
    {
        Container
            .Bind<IControllable>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonReloadingEvent()
    {
        Container
            .Bind<IOnReloadingEvent>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonTransferable()
    {
        Container
            .Bind<ITransferable>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonGameObject()
    {
        Container
            .Bind<CannonBase>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonGetDamageable()
    {
        Container
            .Bind<ICannonGetDamageable>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonDiedEvent()
    {
        Container
            .Bind<IOnCannonDiedEvent>()
            .FromInstance(_cannon)
            .AsSingle();
    }

    private void BindCannonChangedHealthsEvent()
    {
        Container
            .Bind<IOnCannonChangedHealths>()
            .FromInstance(_cannon)
            .AsSingle();
    }
}