using UnityEngine;

public interface IStats
{
    public CannonStats Stats { get; }

    public void Buff(CannonStats stats, float time);

    public void Set(CannonStats stats);
}
