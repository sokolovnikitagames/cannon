using UnityEngine;

public abstract class RotateStrategyBase : IRotateable
{
    protected Transform _cannonTransform;

    public RotateStrategyBase(Transform cannonTransform) 
    {
        _cannonTransform = cannonTransform;
    }

    public abstract void Rotate(Vector2 rotateDirection, float rotateSpeed);
}
