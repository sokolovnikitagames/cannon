using UnityEngine;

public class GeneralRotateStrategy : RotateStrategyBase
{
    public GeneralRotateStrategy(Transform cannonTransform) : base(cannonTransform)
    {
    }

    public override void Rotate(Vector2 rotateDirection, float rotateSpeed)
    {
        Vector3 rotation = new Vector3(rotateDirection.y, rotateDirection.x * -1);
        _cannonTransform.eulerAngles = _cannonTransform.eulerAngles - rotation * rotateSpeed;
    }
}
