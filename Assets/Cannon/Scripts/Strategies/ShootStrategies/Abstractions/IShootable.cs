using UnityEngine;

public interface IShootable
{
    public void Shoot(CannonballFactoryBase cannonballFactory, 
        CannonballFactoryBase.CannonballType cannonballType, Vector3 attackPoint, Quaternion quaternion);
}
