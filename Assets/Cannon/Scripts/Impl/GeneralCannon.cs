using UnityEngine;

public class GeneralCannon : CannonBase
{
    protected override void InitStrategies()
    {
        _rotateStrategy = new GeneralRotateStrategy(transform);
        _shootStrategy = new GeneralCannonShotStrategy();
        _recoilStrategy = new GeneralCannonRecoilStrategy(this, _rigidbody);
        _statsStrategy = new GeneralStatsStrategy();
    }
}
