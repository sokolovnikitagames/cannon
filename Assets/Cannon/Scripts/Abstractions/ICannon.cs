using UnityEngine;

public interface ICannon : IControllable, ICannonTransform, ICannonBuffable, IOnReloadingEvent, 
    ITransferable, ICannonGetDamageable, IOnCannonDiedEvent, IOnCannonChangedHealths
{
    
}
