using System;
using UnityEngine;

public interface IOnCannonChangedHealths
{
    public event Action<float, float> OnChangedHealthsEvent;
}
