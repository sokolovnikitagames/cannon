using UnityEngine;

[CreateAssetMenu(fileName = "New CannonStats", menuName = "Cannon Stats", order = 51)]
public class CannonStats : ScriptableObject
{
    [SerializeField] protected float _reloadSpeed;

    public float ReloadSpeed { get { return _reloadSpeed; } set { _reloadSpeed = value; } } 
}
