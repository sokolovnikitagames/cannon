using UnityEngine;

public class FollowPlayer : MonoBehaviour
{
    [SerializeField] private GameObject _cannon;
    [SerializeField] private Vector3 _positionOffset;
    [SerializeField] private Vector3 _rotationOffset;

    private void Update()
    {
        SetPosition();
        SetRotation();
    }

    private void SetPosition()
    {
        Vector3 position = _cannon.transform.TransformPoint(_positionOffset);
        transform.position = new Vector3(position.x, position.y, position.z);
    }

    private void SetRotation()
    {
        Vector3 rotation = new Vector3(0, _cannon.transform.eulerAngles.y, 0) + _rotationOffset;
        transform.rotation = Quaternion.Euler(rotation);
    }
}
