using System.Collections.Generic;
using UnityEngine;
using static MobFactoryBase;

[CreateAssetMenu(fileName = "New RoundConfig", menuName = "Round Config", order = 51)]
public class RoundConfig : ScriptableObject
{
    [SerializeField] private float _duration; 
    [SerializeField] private List<MobSpawnConfig> _mobSpawnConfigs;

    public float Duration { get { return _duration; } }
    public List<MobSpawnConfig> MobSpawnConfigs { get { return _mobSpawnConfigs; } }
}
