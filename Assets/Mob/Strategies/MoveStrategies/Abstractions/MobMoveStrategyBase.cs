using UnityEngine;

public abstract class MobMoveStrategyBase : IMoveable
{  
    public MobMoveStrategyBase()
    {
        
    }

    public abstract void Move(float speed);
}
