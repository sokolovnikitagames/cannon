using UnityEngine;

public class Slime : MobBase
{
    protected override void InitStrategies()
    {
        _moveStrategy = new ToPlayerMoveStrategy(_mobRigidbody, _cannonTransform.Transform);
        _dieStrategy = new GeneralMobDieStrategy();
    }
}
