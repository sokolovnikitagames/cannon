using UnityEngine;
using Zenject;

public abstract class MobFactoryBase : MonoBehaviour
{
    private DiContainer _diContainer;

    [Inject]
    private void Construct(DiContainer diContainer)
    {
        _diContainer = diContainer;
    }

    public MobBase Spawn(MobType mobType, Vector3 position, Quaternion quaternion)
    {
        MobBase mob = GetPrefab(mobType);
        return _diContainer.InstantiatePrefab(mob, position, quaternion, transform).GetComponent<MobBase>();
    }   

    public enum MobType
    {
        Slime,
        ChestBuff
    }

    protected abstract MobBase GetPrefab(MobType mobType);
}
