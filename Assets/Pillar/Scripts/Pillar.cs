using UnityEngine;
using Zenject;

public class Pillar : MonoBehaviour
{
    [SerializeField] private Transform _cannonPoint;

    private ITransferable _transferableObject;

    [Inject]
    private void Construct(ITransferable transferableObject)
    {
        _transferableObject = transferableObject;
    }

    private void TransferCannon()
    {
        _transferableObject.Transfer(_cannonPoint.position);
    }

    private void OnCollisionEnter(Collision collision)
    {
        CannonballBase cannonball = collision.gameObject.GetComponent<CannonballBase>();
        if (cannonball != null) 
        {
            TransferCannon();
        }
    }
}
